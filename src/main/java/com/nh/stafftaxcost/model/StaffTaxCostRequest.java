package com.nh.stafftaxcost.model;

import com.nh.stafftaxcost.enums.Rank;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StaffTaxCostRequest {
    private String name;

    @Enumerated(value = EnumType.STRING)
    private Rank rank;

    private Integer preTax;

    private Integer fourInsurances;

    private Integer incomeTax;

    private Integer afterSalary;

    private String etcMemo;
}
