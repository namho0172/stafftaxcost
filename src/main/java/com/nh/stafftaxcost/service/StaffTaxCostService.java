package com.nh.stafftaxcost.service;

import com.nh.stafftaxcost.entity.StaffTaxCost;
import com.nh.stafftaxcost.model.StaffTaxCostRequest;
import com.nh.stafftaxcost.repository.StaffTaxCostRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StaffTaxCostService {
    private final StaffTaxCostRepository staffTaxCostRepository;

    public void setStaffTaxCost(StaffTaxCostRequest repository){
        StaffTaxCost addData = new StaffTaxCost();
        addData.setName(addData.getName());
        addData.setRank(addData.getRank());
        addData.setPreTax(addData.getPreTax());
        addData.setFourInsurances(addData.getFourInsurances());
        addData.setIncomeTax(addData.getIncomeTax());
        addData.setAfterSalary(addData.getAfterSalary());
        addData.setEtcMemo(addData.getEtcMemo());

        staffTaxCostRepository.save(addData);
    }
}
