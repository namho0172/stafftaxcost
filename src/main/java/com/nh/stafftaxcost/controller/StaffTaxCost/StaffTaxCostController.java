package com.nh.stafftaxcost.controller.StaffTaxCost;

import com.nh.stafftaxcost.model.StaffTaxCostRequest;
import com.nh.stafftaxcost.service.StaffTaxCostService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/tax")
public class StaffTaxCostController {
    public final StaffTaxCostService staffTaxCostService;

    @PostMapping("/staff")
    public String setStaffTaxCost(@RequestBody StaffTaxCostRequest request){
        staffTaxCostService.setStaffTaxCost(request);

        return "등록완료";
    }
}
