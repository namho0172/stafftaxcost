package com.nh.stafftaxcost.repository;

import com.nh.stafftaxcost.entity.StaffTaxCost;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StaffTaxCostRepository extends JpaRepository <StaffTaxCost, Long> {
}
