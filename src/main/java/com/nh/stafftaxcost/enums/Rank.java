package com.nh.stafftaxcost.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Rank {
    CEO("대표이사")
    , General_Manager("부장")
    , Assist("대리");

    private final String name;
}
