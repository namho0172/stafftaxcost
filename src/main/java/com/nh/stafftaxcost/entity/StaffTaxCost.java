package com.nh.stafftaxcost.entity;

import com.nh.stafftaxcost.enums.Rank;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;

@Entity
@Getter
@Setter
public class StaffTaxCost {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false, length = 15)
    @Enumerated(EnumType.STRING)
    private Rank rank;

    @Column(nullable = false)
    private Integer preTax;

    @Column(nullable = false)
    private Integer fourInsurances;

    @Column(nullable = false)
    private Integer incomeTax;

    @Column(nullable = false)
    private Integer afterSalary;

    @Column(columnDefinition = "TEXT")
    private String etcMemo;
}
